# Airflow tutorial

## Running on NixOS

1. `nix-shell`
2. Create a virtual environment, one off: `python3 -m venv .venv`
3. `AIRFLOW_GPL_UNIDECODE=yes pip install apache-airflow`
4. Apply (I know right...) patch: https://github.com/jd/tenacity/commit/036c7e9ebe397a8127fbee37c2acc0bcb4fc7f11
5. `airflow initdb`
6. Run the webserver `airflow webserver -p $PORT` and scheduler `airflow scheduler`
7. Add a DAG, tasks and profit!
