#!/usr/bin/env bash

mkdir -p $PWD/logs

echo 'Starting Airflow webserver...'
airflow webserver -p 8111 > $PWD/logs/web.log 2>&1 &
echo 'Starting Airflow scheduler...'
airflow scheduler > $PWD/logs/scheduler.log 2>&1 &
echo 'Done.'
