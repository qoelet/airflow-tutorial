with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "airflow-env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    python3

    python37Packages.lxml
    python37Packages.numpy
    python37Packages.pandas
    python37Packages.pip
  ];

  shellHook = ''
    export AIRFLOW_HOME=$PWD/.airflow
    export PATH="~/.local/bin/lsb_release:$PATH"

    alias gst="git status"
    alias vim="nvim"
    source .venv/bin/activate
  '';
}
