from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['foo@bar.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'hello',
    default_args=default_args,
    schedule_interval=timedelta(days=1))

# Tasks
t1 = BashOperator(
    task_id='print_date',
    bash_command='date',
    dag=dag)

t2 = BashOperator(
    task_id='sleep',
    bash_command='sleep 5',
    retries=3,
    dag=dag)

# Templates
foo_command = '''
    {% for i in range(5) %}
        echo "{{ ds }}"
        echo "{{ macros.ds_add(ds, 7) }}"
        echo "{{ params.my_param }}"
    {% endfor %}
'''

t3 = BashOperator(
    task_id='foo',
    bash_command=foo_command,
    params={'my_param': 'Bar Baz'},
    dag=dag)

# Setting dependencies
# t1.set_downstream(t2) # equivalently, t1 >> t2
t2.set_upstream(t1) # t2 << t1
